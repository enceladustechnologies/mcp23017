/*
=====================================================================
File Name          : MCP23017.cpp
Project            : MCP23017
Author             : S. Weekley
Version            : 0.0A
Description    : This contains working code for communicating with
                MCP23017 through I2C.
=====================================================================
*Copyright (C) <2015>  <Enceladus Technologies LLC>
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <stropts.h>
#include <stdio.h>
#include <iostream>
#include <math.h>

#include <string.h>

#ifndef     MCP23017_H
#define     MCP23017_H
// MCP23017-E/SP is the one I'm working with
/*
 * ------------ REGISTER ------------
 */
//The following is for IOCON.BANK = 1

#define     IODIRA_1        0x00    // I/O Direction 1-> input; 0-> output
#define     IPOLA_1         0x01    // Input Polarity Port 1-> Opposite 0 -> Same
#define     GPINTENA_1      0x02    // Interrupt on change 1-> Enable 0-> Disable
#define     DEFVALA_1       0x03    // Default Value
#define	    INTCONA_1       0x04    // Interrupt on Change ???
#define     IOCONA_1        0x05    // I/O Expander Config
#define     GPPUA_1         0x06    // Pull up Resister 1-> Enabled 0-> disabled
#define     INTFA_1         0x07    // Interrupt Flag Reg 1-> Pin interrupt 0-> Not pending
#define     INTCAPA_1       0x08    // Interrupt Capture Value 1-> High 0-> Low
#define     GPIOA_1         0x09    // GPI0 Port register 1-> High 0-> Low
#define     OLATA_1         0x0A    // Output Latch 1-> High 0-> Low

#define     IODIRB_1        0x10    // I/O Direction 1-> input; 0-> output
#define     IPOLB_1	        0x11    // Input Polarity Port 1-> Opposite 0 -> Same
#define     GPINTENB_1      0x12    // Interrupt on change 1-> Enable 0-> Disable
#define     DEFVALB_1       0x13    // Default Value
#define     INTCONB_1       0x14    // Interrupt on Change ???
#define     IOCONB_1        0x15    // I/O Expander Config
#define     GPPUB_1         0x16    // Pull up Resister 1-> Enabled 0-> disabled
#define     INTFB_1         0x17    // Interup Flag Reg 1-> Pin interrupt 0-> Not pending
#define     INTCAPB_1       0x18    // Interrupt Capture Value 1-> High 0-> Low
#define     GPIOB_1         0x19    // GPI0 Port register 1-> High 0-> Low
#define     OLATB_1         0x1A    // Output Latch 1-> High 0-> Low

//The following is for IOCON.BANK = 0
#define     IODIRA_0        0x00    // I/O Direction 1-> input; 0-> output
#define     IODIRB_0        0x01    // I/O Direction 1-> input; 0-> output
#define     IPOLA_0         0x02    // Input Polarity Port 1-> Opposite 0 -> Same
#define     IPOLB_0         0x03    // Input Polarity Port 1-> Opposite 0 -> Same
#define     GPINTENA_0      0x04    // Interrupt on change 1-> Enable 0-> Disable
#define     GPINTENB_0      0x05    // Interrupt on change 1-> Enable 0-> Disable
#define     DEFVALA_0       0x06    // Default Value
#define     DEFVALB_0       0x07    // Default Value
#define	    INTCONA_0       0x08    // Interrupt on Change ???
#define	    INTCONB_0       0x09    // Interrupt on Change ???
#define     IOCONA_0        0x0A    // I/O Expander Config
#define     IOCONB_0        0x0B    // I/O Expander Config
#define     GPPUA_0         0x0C    // Pull up Resister 1-> Enabled 0-> disabled
#define     GPPUB_0         0x0D	// Pull up Resister 1-> Enabled 0-> disabled
#define     INTFA_0         0x0E	// Interrupt Flag Reg 1-> Pin interrupt 0-> Not pending
#define     INTFB_0         0x0F	// Interrupt Flag Reg 1-> Pin interrupt 0-> Not pending
#define     INTCAPA_0       0x10    // Interrupt Capture Value 1-> High 0-> Low
#define     INTCAPB_0       0x11    // Interrupt Capture Value 1-> High 0-> Low
#define     GPIOA_0         0x12    // GPI0 Port register 1-> High 0-> Low
#define     GPIOB_0         0x13    // GPI0 Port register 1-> High 0-> Low
#define     OLATA_0         0x14    // Output Latch 1-> High 0-> Low
#define     OLATB_0         0x15    // Output Latch 1-> High 0-> Low

/*
 * REGISTER		|		R		| 	W		|		R/W
______________________________________________________________
 * IODIR                                             X
 * IPOL                                              X
 * GPINTEN                                           X
 * DEFVAL                                            X
 * INTCON									         X
 * IOCON										     X
 * GPPU							                     X
 * INTF           X
 * INTCAP         X
 * GPIO           								     X
 * OLAT									             X
 *
 */


/*
 * IOCON.BANK
 * BANK: Controls how the registers are addressed
 * 1 = The registers associated with each port are separated into different banks
 * 0 = The registers are in the same bank (addresses are sequential)
 * NOTE: So when the bank control is changed, a different constant must be used
 */

/*
 * See the pdf for exact explanation
 *
 * ------------ Example ------------
 * Goal: Change the polarity of the 8th (bit 7) input pins
 * 1) address of device
 * 	- 0x20
 * 2) address of register
 * 	- 0x02 or 0x12 (depending on which bank sent)
 * 3) data becomes 0b1000 0000 (8th bit only)
 */

/*
 * C++ primatives sizes
 * unsigned charactor 	-> 8 bits
 * unsigned int 				-> 16 bits
 * as a note, the MCP23017 8-bit mode or 16-bit mode via IOCON.BANK
 * lets call it a word for s&g.
 */

using namespace std;

struct Bitsle
{
    unsigned b0:1,b1:1,b2:1,b3:1,b4:1,b5:1,b6:1,b7:1;
    //b8:1,b9:1,b10:1,b11:1,b12:1,b13:1,b14:1,b15:1;
};
union Wordle
{
    Bitsle bits;
    unsigned char byte;
};

struct Bitsbe
{
    unsigned b7:1,b6:1,b5:1,b4:1,b3:1,b2:1,b1:1,b0:1;
    //b15:1,b14:1,b13:1,b12:1,b11:1,b10:1,b9:1,b8:1,
    //b8:1,
};
union Wordbe
{
    Bitsle bits;
    unsigned char byte;
};

class MCP23017{
public:
    MCP23017(int bus, int chipAddrs);							        //Initialization
    ~MCP23017();										                //destructor
    bool writeI2CDataByte(char chipAddrs, char dataAddrs, char value);  //generic write
    bool writeI2CDataByte(char dataAddrs, char value);                  //Writes to specific Chip
    bool setbank0();                                                    //Sets IOCON.bank = 0
    bool setbank1();                                                    //sets IOCON.bank = 1
    bool readI2CDataByte(unsigned char reg);				            //reads data of reg address
    //bool setPullup(char msg);								            //Sets requested pull-up resistors to enabled
    //bool unsetPullup(char msg);  							            //Sets requested pull-up ressistors to disabled
    //bool setOutput(char address, char msg);				            //Set state of Outputs
    //bool getInput(char address, char msg);				            //Get state of Inputs
    //bool seti2cbus(int bus); 								            //Change i2c bus
    /* More Later */

    unsigned char I2CBus; 									            //The bus is static for rpi but can change for others
    unsigned char I2CAddress; 								            //Address of device (chip)
    unsigned char regAddr;									            //reg address
	int IOCONBANK; 											            //State of IOCONBANK (0 or 1)
    int file;											                //file descriptor
    string filename;
    unsigned char rxmsg[2];                                             //data received
    unsigned char txmsg[2];                                             //transmitted data
    union Wordle wordbig;
    union Wordbe wordlit;
};
#endif /* MCP23017_H */
