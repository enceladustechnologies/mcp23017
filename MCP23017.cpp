/*
=====================================================================
 File Name	    : MCP23017.cpp
 Project	    : MCP23017
 Author  	    : S. Weekley
 Version 	    : 0.0A
 Description 	: This contains working code for communicating with
                 MCP23017 through I2C.
=====================================================================
*Copyright (C) <2015>  <Enceladus Technologies LLC.>
*
*This program is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*any later version.
*
*This program is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include "MCP23017.h"

#define MAX_BUS 2 //bytes
#define debug
using namespace std;

MCP23017::MCP23017(int bus, int chipAddrs){
    this->I2CBus = bus;
    this->I2CAddress = chipAddrs;
    IOCONBANK = 0x05; // <-Check this

    //snprintf(filename,sizeof(filename),"/dev/i2c-%d",I2CBus);
    this->filename = "/dev/i2c-" + std::to_string(bus);



    if ((file=open(filename.c_str(), O_RDWR)) <0 ){
        cout << "Failed to open the i2c bus";
        exit(-1);
    }
    if (ioctl(file,I2C_SLAVE,I2CAddress) < 0){
        cout << I2CAddress << "failed." << endl;
        exit(-2);
    }
    close(file);
}

MCP23017::~MCP23017(){
	//TODO: Add Needed code here
	close(this->file);
}

bool MCP23017::writeI2CDataByte(char chipAddrs, char dataAddrs, char value){ //generic write

    memset(this->txmsg,0,sizeof(this->txmsg));
    this->txmsg[0] = dataAddrs;
    this->txmsg[1] = value;


    if ((file=open(filename.c_str(), O_RDWR)) <0 ){
        perror("Failed to open the i2c bus");
        return false;
    }
    if (ioctl(file,I2C_SLAVE,chipAddrs) < 0){
        cout << chipAddrs << "failed." << endl;
        return false;
    }
    if (write(file,txmsg,2) != 2){
        cout << "I2C write failure." << endl;
        return false;
    }
    close(file);
    return true;
}

bool MCP23017::writeI2CDataByte(char dataAddrs, char value){
    /*
     * Writes to address specified at
     * instantiation
     */

    /*
     * How this works
     * 1) Try and open the file
     * 2) mod file with ioctl
     * 3) Compose msg
     */

    memset(this->txmsg,0,sizeof(this->txmsg));
    this->txmsg[0] = dataAddrs;
    this->txmsg[1] = value;


    if ((file=open(filename.c_str(), O_RDWR)) <0 ){
        perror("Failed to open the i2c bus");
        return false;
    }
    if (ioctl(file,I2C_SLAVE,this->I2CAddress) < 0){
        cout << this->I2CAddress << "failed." << endl;
        return false;
    }
    if (write(file,txmsg,2) != 2){
        cout << "I2C write failure." << endl;
        return false;
    }

    //Read the IOCON.BANK state end
    close(file);
    return true;
}

bool MCP23017::setbank0(){
    bool banks[2] = {0,0};
	banks[0] = writeI2CDataByte(I2CAddress,IOCONA_0,0x8);
	banks[1] = writeI2CDataByte(I2CAddress,IOCONB_0,0x8);
	if(banks[0] && banks[1]){
        this->IOCONBANK = 0;
	}

    /*
     *TODO: Probably best to read the state
     *before attempting to write
     */
    return false;
}

bool MCP23017::setbank1(){
    bool banks[2] = {0,0};
	writeI2CDataByte(I2CAddress,IOCONA_1,0x8);
	writeI2CDataByte(I2CAddress,IOCONB_1,0x8);
	if(banks[0] && banks[1]){
        this->IOCONBANK = 1;
	}
	else{
        cout << "ERROR: Unable to set Both banks!" << endl;
        this->IOCONBANK = -1;
	}

    /*
     *TODO: Probably best to read the state
     *before attempting to write
     */
    return false;
}

bool MCP23017::readI2CDataByte(unsigned char reg){
    //register address is already known
    int resp;
    //first write, then read
    memset(this->rxmsg,0,sizeof(this->rxmsg));
    memset(this->txmsg,0,sizeof(this->txmsg));

    if ((file=open(filename.c_str(), O_RDWR)) <0 ){
        perror("Failed to open the i2c bus");
        return false;
    }

    if (ioctl(file,I2C_SLAVE,I2CAddress) < 0){
        cout << I2CAddress << "failed." << endl;
        return false;
    }
    this->txmsg[0] = reg;
    resp = write(file,this->txmsg,1);

    if (resp != 1){
        cout << "No ACK bit! # " << resp << endl;
        return false;
	}
    else {
        resp = read(file,this->rxmsg, 1);
#ifdef debug
        printf("status = (0x%02X)\n\r",(unsigned char)*this->rxmsg);
        /*
         * itoa (i,buffer,2); -> for binary string output
         * itoa (i,buffer,16); -> For binary
         */
#endif
    }
    return true;
}


