# README #

### MCP23017 ###

* This is the official Enceladus Technologies Repository for the MCP23017 I2C communication library.  
* Version: 0.0A (Pre-release)

### How do I get set up? ###

* To set up, clone the repository to your computer, change the code in the main file to fit your personal needs, then compile the code.  
* Please keep in mind that this was written on a Linux device, future upgrades *do* include retooling the code to work with windows devices. 
* The following command was used to compile this library on an Ubuntu Linux device: g++ main.cpp MCP23017.cpp -std=c++0x -Wall -Wextra -o2 -o test
* Please keep in mind that we are a new company, donations are accepted, but this software has been provided to the public under GNU GPL license. 
* For more information see the wiki. 


### Who do I talk to? ###
* For questions concerns and commercial use email Enceladus Technologies at the following email address: entechcustomer at gmail.com