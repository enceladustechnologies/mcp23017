#include <iostream>
#include "MCP23017.h"
using namespace std;

int main()
{
    cout << "This is a test of I2C library!" << endl;
    MCP23017 mcptest(1,0x20);
    cout << "Initialization complete!" << endl;
    // i2cset 1 0x20 0x06 0xff
    mcptest.writeI2CDataByte(0x20,0x00,0xff);
    mcptest.readI2CDataByte(0x06);
    return 0;
}
